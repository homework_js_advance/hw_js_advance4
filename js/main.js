"use strict";


async function fetchData(url) {
    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error(`Помилка отримання даних: ${response.statusText}`);
        }

        return await response.json();
    } catch (error) {
        throw new Error(`Помилка отримання даних: ${error.message}`);
    }
}

async function fetchAndRenderCharacters(characterUrls, container) {
    try {
        const characters = await Promise.all(characterUrls.map(url => fetchData(url)));
        
        const charactersContainer = document.createElement("ul");
        characters.forEach(character => {
            const characterItem = document.createElement("li");
            characterItem.textContent = character.name;
            charactersContainer.appendChild(characterItem);
        });

        container.querySelector('.loading-spinner').style.display = 'none';
        container.appendChild(charactersContainer);

        // Додаємо клас loaded для виклику анімації
        container.querySelector('.film-title').classList.add('loaded');
    } catch (error) {
        console.error(`Помилка отримання даних персонажів: ${error.message}`);
    }
}

async function renderFilms() {
    try {
        const films = await fetchData("https://ajax.test-danit.com/api/swapi/films");
        const filmsListContainer = document.getElementById("filmsList");

        // Впорядковуємо фільми за епізодами від першого до шостого
        films.sort((a, b) => a.episodeId - b.episodeId);

        for (const film of films) {
            const filmElement = document.createElement("div");
            const loadingSpinner = document.createElement("div");
            loadingSpinner.className = "loading-spinner";
            filmElement.innerHTML = `
                <h3 class="film-title">Епізод ${film.episodeId}: ${film.name} ${loadingSpinner.outerHTML}</h3>
                <p>${film.openingCrawl}</p>
                <p>Персонажі:</p>
            `;

            filmsListContainer.appendChild(filmElement);

            await fetchAndRenderCharacters(film.characters, filmElement);
        }
    } catch (error) {
        console.error(`Помилка отримання даних: ${error.message}`);
    }
}

// Викликаємо функцію для рендеру фільмів
renderFilms();